<?php
require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';
$sheep = new Animal("shaun");

echo "Nama: ". $sheep->name ."<br>"; // "shaun"
echo "Jumlah Kaki: ". $sheep->legs ."<br>"; // 2
echo "Cold Blooded?: ". $sheep->cold_blooded ."<br>"; // false

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
// index.php
$sungokong = new Ape("kera sakti");
echo "Nama: ". $sungokong->name ."<br>";
echo "Jumlah Kaki: ". $sungokong->legs ."<br>";
echo "Cold Blooded?: ". $sungokong->cold_blooded ."<br>";
$sungokong->yell() ."<br>"; // "Auooo"

$kodok = new Frog("buduk");
echo "Nama: ". $kodok->name; // "shaun"
echo "Jumlah Kaki: ". $kodok->legs ."<br>"; // 2
echo "Cold Blooded?: ". $kodok->cold_blooded ."<br>";
$kodok->jump() ."<br>";
?>
